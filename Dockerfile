#build image
FROM adoptopenjdk/openjdk15-openj9:aarch64-ubi-minimal-jre-15.0.1_9_openj9-0.23.0

#Copy snapshot and install the maven dependencies.
COPY ./target/koreang-0.0.1-SNAPSHOT.jar /usr/app/

#Set the working directory to /usr/app
WORKDIR /usr/app

RUN sh -c 'touch koreang-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","koreang-0.0.1-SNAPSHOT.jar"]