package com.mk.koreang.service;

import com.mk.koreang.domain.DescriptionDto;
import com.mk.koreang.domain.LanguageCardDto;
import com.mk.koreang.exception.EntityNotFoundException;
import com.mk.koreang.mappers.DataManualMapperImpl;
import com.mk.koreang.mappers.ExampleSentenceMapper;
import com.mk.koreang.mappers.LanguageCardMapper;
import com.mk.koreang.model.Description;
import com.mk.koreang.model.LanguageCard;
import com.mk.koreang.repository.LanguageCardRepository;
import com.mk.koreang.repository.SentenceExampleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.*;

@Slf4j
@Service
public class LanguageCardServiceImpl implements LanguageCardService {

    @Autowired
    private LanguageCardRepository languageCardRepository;

    @Autowired
    private SentenceExampleRepository sentenceExampleRepository;

    /*@Autowired
    private LanguageCardManualMapper languageCardManualMapper;*/

    @Autowired
    private DataManualMapperImpl dataManualMapper;

    @Autowired
    private LanguageCardMapper languageCardMapper;

    @Autowired
    private ExampleSentenceMapper exampleSentenceMapper;


    @Override
    public List<LanguageCardDto> getTranslatedDataDto() {
        List<LanguageCardDto> dtos = new ArrayList<>();
        List<LanguageCard> entities = languageCardRepository.findAll();
        for (LanguageCard e: entities) {
            dtos.add(languageCardMapper.entityToDto(e));
        }
        return dtos;
    }

    @Override
    public List<LanguageCardDto> getWordsByChar(String letter) {
        List<LanguageCardDto> dtos = new ArrayList<>();
        List<LanguageCard> entities = languageCardRepository.findAllByRomanizationFormIsStartingWith(letter);

        for (LanguageCard e: entities) {
            dtos.add(languageCardMapper.entityToDto(e));
        }
        return dtos;
    }

    @Override
    public Set<DescriptionDto> getExamples() {
        Set<Description> entities = new HashSet<>();
        entities.addAll(sentenceExampleRepository.findAll());
        return exampleSentenceMapper.descriptionEntityToDto(entities);
    }

    @Override
    public LanguageCardDto getLanguageCardById(Long id) {
        Optional<LanguageCard> languageCard = languageCardRepository.findById(id);

        if(!languageCard.isPresent()){
            throw new EntityNotFoundException(LanguageCard.class, "id", id.toString());
        }
        return languageCardMapper.entityToDto(languageCard.get());
    }

    @Override
    public HttpStatus saveWords(List<LanguageCardDto> languageCardDto) throws Exception {
        List<Description> descriptions = new ArrayList<>();
        List<LanguageCard> cards = languageCardMapper.dtosToEntities(languageCardDto);
        try{
            languageCardRepository.saveAll(cards);

            for (LanguageCard lc: cards) {
                for (Description d : lc.getDescriptions()) {
                    d.setLanguageCard(lc);
                    descriptions.add(d);
                }
            }
            sentenceExampleRepository.saveAll(descriptions);
        }catch (ConstraintViolationException e){
            throw new Exception("could not save data: " + cards);
        }
        return HttpStatus.OK;
    }

    @Override
    public HttpStatus deleteById(Long id) {
        try {
            sentenceExampleRepository.deleteSentenceExampleByLanguageCardId(id);
            languageCardRepository.deleteById(id);
        }catch (IllegalArgumentException e){
            log.debug("Could not delete entity containing id: ", id);
        }
        return HttpStatus.OK;
    }


    @Override
    public HttpStatus saveWord(LanguageCardDto languageCardDto) throws Exception {
        LanguageCard languageCard = languageCardMapper.dtoToEntity(languageCardDto);
        try{

            languageCardRepository.save(languageCard);

            for (Description d: languageCard.getDescriptions()) {
                d.setLanguageCard(languageCard);
            }

            sentenceExampleRepository.saveAll(languageCard.getDescriptions());
        }catch (ConstraintViolationException e){
                throw new Exception("could not save data: " + languageCard);
        }

        return HttpStatus.OK;
    }



}
