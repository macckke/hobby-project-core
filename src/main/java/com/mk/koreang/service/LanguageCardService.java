package com.mk.koreang.service;


import com.mk.koreang.domain.DescriptionDto;
import com.mk.koreang.domain.LanguageCardDto;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Set;


public interface LanguageCardService {

    List<LanguageCardDto> getTranslatedDataDto();

    List<LanguageCardDto> getWordsByChar(String letter);

    HttpStatus saveWord(LanguageCardDto languageCardDto) throws Exception;

    Set<DescriptionDto> getExamples();

    LanguageCardDto getLanguageCardById(Long id);

    HttpStatus saveWords(List<LanguageCardDto> languageCardDto) throws Exception;

    HttpStatus deleteById(Long id);
}
