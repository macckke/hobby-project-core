package com.mk.koreang.mappers;
import com.mk.koreang.model.LanguageCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataManualMapperImpl  {

    @Autowired
    ExampleSentenceMapper exampleSentenceMapper;

    public LanguageCard dataDtoToEntity(com.mk.koreang.domain.LanguageCardDto languageCardDto) {
        if ( languageCardDto == null ) {
            return null;
        }

       return new LanguageCard(languageCardDto.getEnglish(), languageCardDto.getKorean(), languageCardDto.getRomanizationForm());
    }

    public com.mk.koreang.domain.LanguageCardDto entityDataToDto(LanguageCard languageCard) {
        if ( languageCard == null ) {
            return null;
        }

        return new com.mk.koreang.domain.LanguageCardDto(languageCard.getEnglish(), languageCard.getKorean(), languageCard.getRomanizationForm());
    }
}
