package com.mk.koreang.mappers;

import com.mk.koreang.domain.DescriptionDto;
import com.mk.koreang.model.Description;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ExampleSentenceMapper {

    public ExampleSentenceMapper() {
    }

    public Set<DescriptionDto> descriptionEntityToDto(Set<Description> descriptionEntity) {
        if ( descriptionEntity == null ) {
            return null;
        }

        Set<DescriptionDto> dtos = new HashSet<>();

        for (Description de: descriptionEntity) {
            dtos.add(new DescriptionDto(de.getId(), de.getEnglishSentence(), de.getKoreanSentence()));
        }

      return dtos;
    }

    Set<Description> descriptionDtoToEntity(Set<DescriptionDto> descriptionDto) {
        if ( descriptionDto == null ) {
            return null;
        }
        Set<Description> entities = new HashSet<>();

        for (DescriptionDto dd: descriptionDto) {
            entities.add(new Description(dd.getEnglishSentence(), dd.getKoreanSentence()));
        }

        return entities;
    }

    public DescriptionDto descriptionEntityToDto(Description description) {
        if ( description == null ) {
            return null;
        }
        return new DescriptionDto(description.getId(), description.getEnglishSentence(), description.getKoreanSentence());
    }

    Description descriptionDtoToEntity(DescriptionDto descriptionDto) {
        if ( descriptionDto == null ) {
            return null;
        }
        return new Description(descriptionDto.getEnglishSentence(), descriptionDto.getEnglishSentence());
    }
}
