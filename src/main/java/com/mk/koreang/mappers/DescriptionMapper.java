package com.mk.koreang.mappers;

import com.mk.koreang.domain.DescriptionDto;
import com.mk.koreang.model.Description;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper
public interface DescriptionMapper {
    Set<DescriptionDto> descriptionEntityToDto(Set<Description> descriptionEntity);
    Set<Description> descriptionDtoToEntity(Set<DescriptionDto> descriptionDto);
    DescriptionDto descriptionEntityToDto(Description description);
    Description descriptionDtoToEntity(DescriptionDto descriptionDto);
}
