package com.mk.koreang.mappers.manualmappers;


import com.mk.koreang.domain.LanguageCardDto;
import com.mk.koreang.model.LanguageCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LanguageCardManualMapper {

   /* @Autowired
    ExampleSentenceMapper exampleSentenceMapper;

    public LanguageCardManualMapper() {
    }

    public LanguageCard dtoToEntity(LanguageCardDto languageCardDto){
        if(languageCardDto == null) {
            return null;
        }

        LanguageCard languageCard = new LanguageCard();
        languageCard.setEnglish(languageCardDto.getEnglish());
        languageCard.setKorean(languageCardDto.getKorean());
        languageCard.setRomanizationForm(languageCardDto.getRomanizationForm());
        languageCard.setDescriptions(exampleSentenceMapper.descriptionDtoToEntity(languageCardDto.getDescriptions()));

        return languageCard;
    }

    public LanguageCardDto entityToDto(LanguageCard languageCard){
        if(languageCard == null) {
            return null;
        }

        LanguageCardDto languageCardDto = new LanguageCardDto();
        languageCardDto.setId(languageCard.getId());
        languageCardDto.setEnglish(languageCard.getEnglish());
        languageCardDto.setKorean(languageCard.getKorean());
        languageCardDto.setRomanizationForm(languageCard.getRomanizationForm());
        languageCardDto.setDescriptions(exampleSentenceMapper.descriptionEntityToDto(languageCard.getDescriptions()));

        return languageCardDto;
    }

    public List<LanguageCard> dtosToEntities(List<LanguageCardDto> languageCardDtos) {
        if(languageCardDtos == null){
            return null;
        }

        List<LanguageCard> cards = new ArrayList<>();

        for (LanguageCardDto languageCardDto: languageCardDtos) {
            LanguageCard languageCard = new LanguageCard();
            languageCard.setEnglish(languageCardDto.getEnglish());
            languageCard.setKorean(languageCardDto.getKorean());
            languageCard.setRomanizationForm(languageCardDto.getRomanizationForm());
            languageCard.setDescriptions(exampleSentenceMapper.descriptionDtoToEntity(languageCardDto.getDescriptions()));

            cards.add(languageCard);
        }

        return cards;
    }

    public List<LanguageCardDto> entitiesToDtos(List<LanguageCard> languageCards){
        if(languageCards == null){
            return null;
        }

        List<LanguageCardDto> cards = new ArrayList<>();

        for (LanguageCard languageCard: languageCards) {
            LanguageCardDto languageCardDto = new LanguageCardDto();
            languageCardDto.setId(languageCard.getId());
            languageCardDto.setEnglish(languageCard.getEnglish());
            languageCardDto.setKorean(languageCard.getKorean());
            languageCardDto.setRomanizationForm(languageCard.getRomanizationForm());
            languageCardDto.setDescriptions(exampleSentenceMapper.descriptionEntityToDto(languageCard.getDescriptions()));

            cards.add(languageCardDto);
        }

        return cards;
    }*/

}
