package com.mk.koreang.mappers;

import com.mk.koreang.domain.LanguageCardDto;
import com.mk.koreang.model.LanguageCard;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses=ExampleSentenceMapper.class)
public interface LanguageCardMapper {

    LanguageCard dtoToEntity(LanguageCardDto languageCardDto);
    LanguageCardDto entityToDto(LanguageCard languageCard);

    List<LanguageCard> dtosToEntities(List<LanguageCardDto> languageCardDtos);
    List<LanguageCardDto> entitiesToDtos(List<LanguageCard> languageCards);

}
