package com.mk.koreang.repository;


import com.mk.koreang.model.Description;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface SentenceExampleRepository extends JpaRepository<Description, Long> {


    /**
     * Whenever you are trying to modify a record in db, you have to mark it @Transactional as well as @Modifying,
     * which instruct Spring that it can modify existing records.
     * The repository method must be void or the exception keeps getting thrown.
     * @param id
     * @return
     */
    @Transactional
    @Modifying
    @Query("DELETE FROM Description as e WHERE e.languageCard.id = :id")
    void deleteSentenceExampleByLanguageCardId(@Param("id") Long id);
}
