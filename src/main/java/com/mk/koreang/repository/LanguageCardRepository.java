package com.mk.koreang.repository;
import com.mk.koreang.model.LanguageCard;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LanguageCardRepository extends JpaRepository<LanguageCard, Long> {
        List<LanguageCard> findAllByRomanizationFormIsStartingWith(String letter);

        /*@Query("FROM LanguageCard lc JOIN lc.descriptions d WHERE lc.language_card_id = d.id")
        List<LanguageCard> findAll();*/
}
