package com.mk.koreang.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Description {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;

    @Column(name = "english_sentence")
    private String englishSentence;

    @Column(name = "korean_sentence")
    @Nationalized
    private String koreanSentence;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_card_id", nullable = false)
    private LanguageCard languageCard;


    public Description(){}

    public Description(String englishSentence, String koreanSentence) {
        this.englishSentence = englishSentence;
        this.koreanSentence = koreanSentence;
    }

    public Description(String englishSentence, String koreanSentence, LanguageCard languageCard) {
        this.englishSentence = englishSentence;
        this.koreanSentence = koreanSentence;
        this.languageCard = languageCard;
    }

}
