package com.mk.koreang.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Nationalized;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;



@Entity
@Table(name = "language_card",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"english", "korean"}))
@Setter
@Getter
public class LanguageCard implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue
    private Long id;

    @Column(name = "english", nullable = false)
    private String english;

    @Column(name = "korean", nullable = false)
    @Nationalized
    private String korean;

    @Column(name = "romanization_form", nullable = false)
    private String romanizationForm;

    /**
     * The @JoinColumn annotation allows you to specify the Foreign Key column name.
     * Also, it’s very important to set the fetch strategy explicitly to FetchType.LAZY.
     * By default, @ManyToOne associations use the FetchType.EAGER strategy,
     * which can lead to N+1 query issues or fetching more data than necessary.
     */
    @OneToMany(mappedBy = "languageCard", fetch = FetchType.EAGER)
    private Set<Description> descriptions;

    public LanguageCard() {
    }

    public LanguageCard(String english, String korean, String romanizationForm) {
        this.english = english;
        this.korean = korean;
        this.romanizationForm = romanizationForm;
    }

    public LanguageCard(String english, String korean, String romanizationForm, Set<Description> descriptions) {
        this.english = english;
        this.korean = korean;
        this.romanizationForm = romanizationForm;
        this.descriptions = descriptions;
    }

    public LanguageCard(Long id, String english, String korean, String romanizationForm, Set<Description> descriptions) {
        this.id = id;
        this.english = english;
        this.korean = korean;
        this.romanizationForm = romanizationForm;
        this.descriptions = descriptions;
    }

}
