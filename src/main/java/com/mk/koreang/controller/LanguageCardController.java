package com.mk.koreang.controller;


import com.mk.koreang.domain.DescriptionDto;
import com.mk.koreang.domain.LanguageCardDto;
import com.mk.koreang.service.LanguageCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * making this a bidirectional relationship between examples and words table.
 * Bidirectional means that we are able to access examples from words, and also words from examples.
 */


@RestController
@RequestMapping("/data/v1")
public class LanguageCardController {

    @Autowired
    LanguageCardService languageCardService;

    @GetMapping("/words")
    List<LanguageCardDto> getData(){
        //todo call service to get the desired data
        return languageCardService.getTranslatedDataDto();
    }

    @GetMapping("/words/{id}")
    LanguageCardDto getData(@PathVariable Long id){
        return languageCardService.getLanguageCardById(id);
    }

    @GetMapping("/search/korean/{letter}")
    List<LanguageCardDto> searchByKoreanChar(@PathVariable String letter){
        //todo call service to get the desired data
        return languageCardService.getWordsByChar(letter);
    }

    @GetMapping("/search/english/{letter}")
    List<LanguageCardDto> searchByEnglishChar(@PathVariable String letter){
        //todo call service to get the desired data
        return languageCardService.getWordsByChar(letter);
    }

    @GetMapping("/examples")
    Set<DescriptionDto> getAllExamples(){
        return languageCardService.getExamples();
    }

    @PostMapping("/save")
    HttpStatus saveWord(@RequestBody LanguageCardDto languageCardDto) throws Exception {
        return languageCardService.saveWord(languageCardDto);
    }

    @PostMapping("/save/cards")
    HttpStatus saveWord(@RequestBody List<LanguageCardDto> languageCards) throws Exception {
        return languageCardService.saveWords(languageCards);
    }

    @DeleteMapping("delete/{id}")
    HttpStatus deleteLanguageCard(@PathVariable Long id){
        return languageCardService.deleteById(id);
    }
}
