package com.mk.koreang.domain;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Set;


@Component
@Setter
@Getter
public class LanguageCardDto {
    private Long id;
    private String english;
    private String korean;
    private String romanizationForm;

    private Set<DescriptionDto> descriptions;


    public LanguageCardDto() {
    }

    public LanguageCardDto(String english, String korean, String romanizationForm) {
        this.english = english;
        this.korean = korean;
        this.romanizationForm = romanizationForm;
    }

    public LanguageCardDto(Long id, String english, String korean, String romanizationForm) {
        this.id = id;
        this.english = english;
        this.korean = korean;
        this.romanizationForm = romanizationForm;
    }
}
