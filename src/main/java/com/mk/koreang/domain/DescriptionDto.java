package com.mk.koreang.domain;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Setter
@Getter
public class DescriptionDto {

    private Long id;
    private String englishSentence;
    private String koreanSentence;

    public DescriptionDto() {
    }

    public DescriptionDto(String englishSentence, String koreanSentence) {
        this.englishSentence = englishSentence;
        this.koreanSentence = koreanSentence;
    }

    public DescriptionDto(Long id, String englishSentence, String koreanSentence) {
        this.id = id;
        this.englishSentence = englishSentence;
        this.koreanSentence = koreanSentence;
    }
}
