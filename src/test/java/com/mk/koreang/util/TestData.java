package com.mk.koreang.util;

import com.mk.koreang.model.Description;
import com.mk.koreang.model.LanguageCard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestData {
    public static List<LanguageCard> generateLanguageCardDtos(int i){
        List<LanguageCard> cards = new ArrayList<>();
        for (int j = 0; j < i; j++) {
            cards.add(createLanguageCard(j));
        }
        return cards;
    }

    public static LanguageCard createLanguageCard(int i) {
        LanguageCard languageCard = new LanguageCard("hello" + i
                ,"안녕하세요" + i,"annyeonghaseyo" + i);

        languageCard.setDescriptions(createDescriptions(i, languageCard));

        return languageCard;
    }

    public static Set<Description> createDescriptions(int i, LanguageCard languageCard){
        Set<Description> descriptionCards = new HashSet<>();

        descriptionCards.add(new Description(
                "hello, how are you? " + i,
                "안녕하세요, 잘지내세요? " + i,
                languageCard));

        return descriptionCards;
    }


}
