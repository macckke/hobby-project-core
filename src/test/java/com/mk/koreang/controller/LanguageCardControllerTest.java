package com.mk.koreang.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class LanguageCardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllWords() throws Exception {
        mockMvc
                .perform(get("/data/v1/words"))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllExamples() throws Exception {
        mockMvc
                .perform(get("/data/v1/examples"))
                .andExpect(status().isOk());
    }


}
