package com.mk.koreang.service;

import com.mk.koreang.domain.LanguageCardDto;
import com.mk.koreang.mappers.ExampleSentenceMapper;
import com.mk.koreang.mappers.LanguageCardMapper;
import com.mk.koreang.repository.LanguageCardRepository;
import com.mk.koreang.util.TestData;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LanguageCardServiceImplTest {

    @Mock
    LanguageCardRepository languageCardRepository;

    @InjectMocks
    ExampleSentenceMapper exampleSentenceMapper;

    @Spy
    LanguageCardMapper languageCardMapper;

    /**
     * This tells mockito to create an instance of the service and inject it here.
     */
    @InjectMocks
    LanguageCardServiceImpl languageCardService;

    @Test
    @DisplayName("find all language cards from in db")
    void test_findAll() {
        when(languageCardRepository.findAll()).thenReturn(TestData.generateLanguageCardDtos(3));
        List<LanguageCardDto> cards = languageCardService.getTranslatedDataDto();

        //assert
        assertFalse(cards.isEmpty());
        assertEquals(cards.size(), 3);
        //verify
        verify(languageCardRepository, times(1)).findAll();
    }

}
