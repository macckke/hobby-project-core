package com.mk.koreang.repository;

import com.mk.koreang.model.Description;
import com.mk.koreang.model.LanguageCard;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
//@DataJpaTest // will set up spring application context for us.
@ActiveProfiles(profiles = {"test"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LanguageCardRepositoryTest {

    @Autowired
    LanguageCardRepository languageCardRepository;

    @Autowired
    SentenceExampleRepository sentenceExampleRepository;


    LanguageCard languageCard;
    Set<Description> descriptions;

    @BeforeAll
    void before() {
        languageCard = new LanguageCard("hello", "안녕하세요", "annyeonghaseyo");
        descriptions = new HashSet<>();
        descriptions.add(new Description("hello, how are you?", "안녕, 잘지냈어?", languageCard));
        languageCardRepository.save(languageCard);
        sentenceExampleRepository.saveAll(descriptions);
    }


    @Test
    void injectedComponentsAreNotNull(){
        assertNotNull(languageCardRepository);
        assertNotNull(sentenceExampleRepository);
    }

    @Test
    @DisplayName("find all language card")
    void test_fetchAll() {
        List<LanguageCard> languageCards = languageCardRepository.findAll();
        assertTrue(!languageCards.isEmpty());
        assertNotNull(languageCards.get(0).getEnglish());
        assertNotNull(languageCards.get(0).getKorean());
        assertNotNull(languageCards.get(0).getRomanizationForm());
    }

   /* @Test
    @DisplayName("delete a language card")
    void test_delete() {
        List<LanguageCard> languageCards = languageCardRepository.findAll();
        assertTrue(!languageCards.isEmpty());
        assertTrue(languageCards.size() == 1);
        //delete card
       sentenceExampleRepository.deleteAll(descriptions);
       List<Description> descriptions = sentenceExampleRepository.findAll();
       assertTrue(descriptions.size() == 0);

        languageCards = languageCardRepository.findAll();
        languageCardRepository.deleteAll(languageCards);
        assertTrue(languageCards.size() == 0);
    }*/

    @Test
    @DisplayName("delete descriptions")
    void test_deleteDescriptions(){
        List<LanguageCard> languageCards = languageCardRepository.findAll();
        assertTrue(!languageCards.isEmpty());
        assertTrue(languageCards.size() == 1);
        //delete card
        sentenceExampleRepository.deleteAll(descriptions);
        List<Description> descriptions = sentenceExampleRepository.findAll();
        assertTrue(descriptions.size() == 0);
    }
}
